import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import math

#
# This file contains the definition of the models used in the experiments
#

# number of pixels in a CelebA image 
NUM_PIXELS = 3*64*64

# HeNormal initialization, from arXiv:1502.01852
def init_tensor_henormal(tensor, scale=1.0):
    size = tensor.size()
    if len(size) < 2:
        return
    rfield = 1
    for dim in size[2:]:
        rfield *= dim
    fan_in = size[1] * rfield
    scale *= math.sqrt(2. / fan_in)
    tensor.normal_(0, scale)

def init_henormal(module, scale=1.0):
    if type(module) in [nn.Linear, nn.Conv2d, nn.ConvTranspose2d]:
        init_tensor_henormal(module.weight.data, scale)

# Orthogonal regularization, from arXiv:1609.07093
def ortho_regul(w):
    channels = w.size(0)
    rw = w.view(channels, -1)
    i = torch.eye(channels)
    if rw.is_cuda:
        i = i.cuda()
    i = Variable(i)
    return torch.sum((torch.matmul(rw, rw.t()) - i)**2)

def sample_gaussian(mu, log_sigma):
    # Using reparameterization trick to sample from a gaussian
    # of variance sigma
    eps = torch.randn(mu.size())
    if mu.is_cuda:
        eps = eps.cuda()
    eps = Variable(eps)
    return mu + torch.exp(log_sigma) * eps

class Generator(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(512, 1024)
        self.fc2 = nn.Linear(1024, 256*8*8)
        # image is 256 x 8 x 8
        self.deconv1 = nn.ConvTranspose2d(256, 128, 4, 2, 1)
        # now 128 x 16 x 16
        self.conv2 = nn.Conv2d(128, 128, 3, 1, 1)
        # now 128 x 16 x 16
        self.deconv3 = nn.ConvTranspose2d(128, 64, 4, 2, 1)
        # now 64 x 32 x 32
        self.deconv4 = nn.ConvTranspose2d( 64, 32, 4, 2, 1)
        # now 32 x 64 x 64
        self.conv5 = nn.Conv2d(32, 3, 3, 1, 1)
        # now 3 x 64 x 64
    
    def forward(self, z):
        h = F.elu(self.fc1(z))
        h = F.elu(self.fc2(h))
        h = h.view((-1, 256, 8, 8))
        h = F.elu(self.deconv1(h))
        h = F.elu(self.conv2(h))
        h = F.elu(self.deconv3(h))
        h = F.elu(self.deconv4(h))
        x = F.tanh(self.conv5(h))
        return x

class Discriminator(nn.Module):
    def __init__(self):
        super().__init__()
        # input is 3 x 64 x 64
        self.conv1 = nn.Conv2d(  3,  48, 4, 2, 1)
        # now 48 x 32 x 32
        self.conv2 = nn.Conv2d( 48,  96, 5, 1, 2)
        # now 96 x 32 x 32
        self.conv3 = nn.Conv2d( 96, 192, 4, 2, 1)
        # now 192 x 16 x 16
        self.conv4 = nn.Conv2d(192, 192, 5, 1, 2)
        # now 192 x 16 x 16
        self.conv5 = nn.Conv2d(192, 384, 4, 2, 1)
        # now 384 x 8 x 8
        self.fc1 = nn.Linear(384*8*8, 1536)
        self.fc2 = nn.Linear(1536, 192)
        self.fc3 = nn.Linear(192, 1)
    
    # the noise input determines on which layer the gaussian noise should
    # be added if any, and log_sigma is the log-std of this noise
    def forward(self, x, noise=None, log_sigma=0.0):
        h = x
        if noise == 0:
            h = sample_gaussian(h, log_sigma)
        h = self.conv1(x)
        if noise == 1:
            h = sample_gaussian(h, log_sigma)
        h = self.conv2(F.elu(h))
        if noise == 2:
            h = sample_gaussian(h, log_sigma)
        h = self.conv3(F.elu(h))
        if noise == 3:
            h = sample_gaussian(h, log_sigma)
        h = self.conv4(F.elu(h))
        if noise == 4:
            h = sample_gaussian(h, log_sigma)
        h = self.conv5(F.elu(h))
        if noise == 5:
            h = sample_gaussian(h, log_sigma)
        h = F.elu(h)
        h = h.view(-1, 384*8*8)
        h = F.elu(self.fc1(h))
        h = F.elu(self.fc2(h))
        y = self.fc3(h)
        return y.view(-1, 1).squeeze(1)

    # compute the regulation loss
    def regul(self):
        return sum(ortho_regul(l.weight) for l in [self.conv1, self.conv2, self.conv3, self.conv4, self.conv5, self.fc1, self.fc2, self.fc3])
    
    # compute projection of the input on a given intermediate layer
    # for perceptual loss
    def adversarial_project(self, x, depth=0):
        assert(depth>=0)
        if depth == 0:
            return x.view(-1, 3*64*64)
        h1 = self.conv1(x)
        if depth == 1:
            return h1.view(-1, 48*32*32)
        h2 = self.conv2(F.elu(h1))
        if depth == 2:
            return h2.view(-1, 96*32*32)
        h3 = self.conv3(F.elu(h2))
        if depth == 3:
            return h3.view(-1, 192*16*16)
        h4 = self.conv4(F.elu(h3))
        if depth == 4:
            return h4.view(-1, 192*16*16)
        h5 = self.conv5(F.elu(h4))
        return h5.view(-1, 384*8*8)

class Encoder(nn.Module):
    def __init__(self):
        super().__init__()
        # input is 3 x 64 x 64
        self.conv1 = nn.Conv2d(  3,  32, 3, 1, 1)
        # now 32 x 64 x 64
        self.conv2 = nn.Conv2d( 32,  64, 4, 2, 1)
        # now 64 x 32 x 32
        self.conv3 = nn.Conv2d( 64, 128, 4, 2, 1)
        # now 128 x 16 x 16
        self.conv4 = nn.Conv2d(128, 128, 5, 1, 2)
        # now 128 x 16 x 16
        self.conv5 = nn.Conv2d(128, 256, 4, 2, 1)
        # now 256 x 8 x 8
        self.fc1 = nn.Linear(256*8*8, 1024)
        self.fc_mu = nn.Linear(1024, 512)
        self.fc_sigma = nn.Linear(1024, 512)
    
    def forward(self, x):
        h = F.elu(self.conv1(x))
        h = F.elu(self.conv2(h))
        h = F.elu(self.conv3(h))
        h = F.elu(self.conv4(h))
        h = F.elu(self.conv5(h))
        h = h.view(-1, 256*8*8)
        h = F.elu(self.fc1(h))
        mu = self.fc_mu(h)
        sigma = self.fc_sigma(h)
        return (mu, sigma)
