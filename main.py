import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data
import torchvision.transforms as transforms
import torch.optim as optim
import torchvision.utils as vutils
import sys
import argparse
import os
import math

from models import Generator, Discriminator, Encoder, init_henormal, NUM_PIXELS
from folder import ImageFolder

parser = argparse.ArgumentParser()
parser.add_argument('--dataroot', required=True, help='path to dataset root')
parser.add_argument('--outdir', required=True, help='path to output directory')
parser.add_argument('--batchsize', type=int, default=64, help='input batch size')
parser.add_argument('--epochs', type=int, default=25, help='number of epochs to train for')
parser.add_argument('--alpha', type=float, default=0.00002, help='alpha for adam. default=0.00002')
parser.add_argument('--disc_alpha', type=float, default=0.00002, help='alpha for discriminator\'s adam. default=0.00002')
parser.add_argument('--beta1', type=float, default=0.5, help='beta1 for adam. default=0.7')
parser.add_argument('--beta2', type=float, default=0.9, help='beta2 for adam. default=0.999')
parser.add_argument('--reload', type=int, help='reload from save number to continue training (starts with this epoch number)')
parser.add_argument('--imgdump', type=int, help='period of image dump (in iterations). no dump if absent')
parser.add_argument('--modeldump', type=int, help='period of model dump (in epoch). no dump is absent')
parser.add_argument('--cuda', action='store_true', help='enables cuda')
parser.add_argument('--hescale', type=float, default=1.0, help='scale factor for henormal init. default=1.0')
parser.add_argument('--disc_adv_depth', type=int, default=0, help='depth at which compute the adversarial metric')
parser.add_argument('--disc_adv_lambdas', type=float, nargs='+', help='lambda factors for combining several layers')
parser.add_argument('--disc_ortho', type=float, default='0.0', help='weight of orthogonal regularisation for discriminator')
parser.add_argument('--use_vae', action='store_true', help='use the vae loss')
parser.add_argument('--use_gan', action='store_true', help='use the gan loss')
parser.add_argument('--train_disc_on_rec', action='store_true', help='show reconstructed images to the discriminator')
parser.add_argument('--use_gan_on_rec', action='store_true', help='use the gan loss on reconstructed images')
parser.add_argument('--use_noise_on_gen', action='store_true', help='apply the VAE noise model on GAN generation')
parser.add_argument('--n_disc_train', type=int, default=1, help='number of trainig of the discriminator compared to generator')
opt = parser.parse_args()

print("Using output directory: {}".format(opt.outdir))

os.makedirs(opt.outdir, exist_ok=True)

enc = Encoder()
gen = Generator()
disc = Discriminator()

if opt.reload is not None:
    enc.load_state_dict("{}/enc-{}.npz".format(opt.dataroot, opt.reload))
    gen.load_state_dict("{}/gen-{}.npz".format(opt.dataroot, opt.reload))
    disc.load_state_dict("{}/disc-{}.npz".format(opt.dataroot, opt.reload))
else:
    init = lambda w: init_henormal(w, opt.hescale)
    enc.apply(init)
    gen.apply(init)
    disc.apply(init)

#
# Load the datasets
#

dataroot = '{}/img_align_celeba'.format(opt.dataroot)
transform = transforms.Compose([
    transforms.CenterCrop(108),
    transforms.Resize(64),
    transforms.ToTensor(),
])

# we use the last 5% of the dataset for validation metrics

N = 202599
valid_limit = int(0.95*N)
def is_validation(fname):
    # filename = ####.jpg
    [number, _] = fname.split('.')
    return (int(number) > valid_limit)

train_dataset = ImageFolder(root=dataroot, transform=transform, criterion=lambda fname: not is_validation(fname))
train_dataloader = torch.utils.data.DataLoader(
    train_dataset,
    batch_size=opt.batchsize,
    shuffle=True,
    num_workers=2
)

valid_dataset = ImageFolder(root=dataroot, transform=transform, criterion=lambda fname: is_validation(fname))
valid_dataloader = torch.utils.data.DataLoader(
    valid_dataset,
    batch_size=opt.batchsize,
    shuffle=True,
    num_workers=2
)

# losses
def kl_criterion(mu, log_var):
    # KL(Gaussian(mu, sigma) || Gaussian(0, 1))
    return 0.5 * torch.sum(torch.exp(log_var) + mu**2 - log_var - 1.0, dim=1).mean()

if opt.cuda:
    enc.cuda()
    gen.cuda()
    disc.cuda()

# setup optimizer
opti_enc = optim.Adam(enc.parameters(), lr=opt.alpha, betas=(opt.beta1, opt.beta2))
opti_gen = optim.Adam(gen.parameters(), lr=opt.alpha, betas=(opt.beta1, opt.beta2))
opti_disc = optim.Adam(disc.parameters(), lr=opt.disc_alpha, betas=(opt.beta1, opt.beta2))

def sample_z(mu, log_var):
    # Using reparameterization trick to sample from a gaussian
    eps = torch.randn(mu.size())
    if opt.cuda:
        eps = eps.cuda()
    eps = Variable(eps)
    return mu + torch.exp(log_var / 2) * eps

def cuda_or_not(v):
    if opt.cuda:
        v = v.cuda()
    return Variable(v)

fixed_z = cuda_or_not(torch.FloatTensor(opt.batchsize, 512).normal_(0, 1))
fixed_z.volatile = True

stats_suffix = ''
start_from = 1
if opt.reload is not None:
    stats_suffix='-from-{}'.format(opt.reload)
    start_from = opt.reload

# function to process a batch of data
def do_batch(i, data, train=False):
        enc.zero_grad()
        gen.zero_grad()

        # retrieve real data
        real_x = cuda_or_not(data[0]*2.0-1.0)
        batch_size = real_x.size(0)
        label_real = cuda_or_not(torch.FloatTensor(batch_size).fill_(1.0))
        label_fake = cuda_or_not(torch.FloatTensor(batch_size).fill_(0.0))

        # encode it
        enc_mu, enc_log_var = enc(real_x)
        # Variational KL loss
        kl_loss = kl_criterion(enc_mu, enc_log_var)
        rec_z = sample_z(enc_mu, enc_log_var)
        # rebuild the image
        rec_x = gen(rec_z)

        # generate a fake one
        gen_z = cuda_or_not(torch.FloatTensor(batch_size, 512).normal_(0, 1)) #* avg_sigma + avg_mu
        gen_x = gen(gen_z)

        # compute ae loss
        if not opt.disc_adv_lambdas:
            adv_real = disc.adversarial_project(real_x, opt.disc_adv_depth).detach()
            adv_rec = disc.adversarial_project(rec_x, opt.disc_adv_depth)
            log_p_x_z = NUM_PIXELS * (-0.5 * ((adv_rec-adv_real)**2).mean() * torch.exp(-2*gen.get_log_sigma()) - 2*gen.get_log_sigma())
        else:
            log_p_x_z = - NUM_PIXELS * 2 * gen.get_log_sigma()
            for (i, l) in enumerate(opt.disc_adv_lambdas):
                adv_real = disc.adversarial_project(real_x, i).detach()
                adv_rec = disc.adversarial_project(rec_x, i)
                log_p_x_z -= 0.5 * l * NUM_PIXELS * ((adv_rec-adv_real)**2).mean() * torch.exp(-2*gen.get_log_sigma())

        # compute discriminator output on gen and rec
        if opt.use_noise_on_gen:
            disc_gen_presig = disc(gen_x, noise=opt.disc_adv_depth, log_sigma=gen.get_log_sigma()).mean()
            disc_rec_presig = disc(rec_x, noise=opt.disc_adv_depth, log_sigma=gen.get_log_sigma()).mean()
        else:
            disc_gen_presig = disc(gen_x).mean()
            disc_rec_presig = disc(rec_x).mean()

        # log((1-D)/D) = p_theta/p_D = - disc_presig

        total_generator_loss = 0.0
        if opt.use_vae:
            total_generator_loss += kl_loss - log_p_x_z
            if opt.use_gan_on_rec:
                total_generator_loss -= disc_rec_presig
        if opt.use_gan:
            total_generator_loss -= disc_gen_presig

        # update encoder and generator
        if train and i % opt.n_disc_train == 0:
            total_generator_loss.backward()
            opti_enc.step()
            opti_gen.step()

        # compute discriminator loss
        # log(D(x)) = softplus(-disc_presig)
        # log(1-D(x)) = softplus(disc_presig)
        disc.zero_grad()
        gan_disc_loss_real = F.softplus(-disc(real_x)).mean()
        gen_x.detach_()
        rec_x.detach_()
        if opt.use_noise_on_gen:
            gan_disc_loss_gen = F.softplus(disc(gen_x, noise=opt.disc_adv_depth, log_sigma=gen.get_log_sigma())).mean()
            gan_disc_loss_rec = F.softplus(disc(rec_x, noise=opt.disc_adv_depth, log_sigma=gen.get_log_sigma())).mean()
        else:
            gan_disc_loss_gen = F.softplus(disc(gen_x)).mean()
            gan_disc_loss_rec = F.softplus(disc(rec_x)).mean()
        disc_regul_loss = opt.disc_ortho * disc.regul()
        gan_disc_loss = gan_disc_loss_real + gan_disc_loss_gen
        if opt.train_disc_on_rec:
            gan_disc_loss += gan_disc_loss_rec
        if train:
            (disc_regul_loss + gan_disc_loss).backward()
            opti_disc.step()

        # return the losses for monitoring
        return (
            [
                kl_loss.data[0],
                (-log_p_x_z).data[0],
                (-disc_rec_presig).data[0],
                (-disc_gen_presig).data[0],
                total_generator_loss.data[0],
                torch.exp(gen.get_log_sigma()).data[0],
                gan_disc_loss_real.data[0],
                gan_disc_loss_gen.data[0],
                gan_disc_loss_rec.data[0],
                gan_disc_loss.data[0],
                disc_regul_loss.data[0],
            ],
            rec_x.data
        )


with open('{}/{}stats.csv'.format(opt.outdir, stats_suffix), 'w') as log_file:
    with open('{}/{}stats-valid.csv'.format(opt.outdir, stats_suffix), 'w') as valid_log_file:

        for epoch in range(start_from, opt.epochs+start_from):
            for i, data in enumerate(train_dataloader, 0):
                (batch_metrics, rec_x) = do_batch(i, data, train=True)
                metrics = [ float(epoch-1) + float(i)/len(train_dataloader) ] + batch_metrics
                print(",".join("%.6f" % v for v in metrics), file=log_file, flush=True)
                if opt.imgdump is not None and i % opt.imgdump == 0:
                    vutils.save_image(data[0],
                            '{}/{:03d}-real.png'.format(opt.outdir, epoch),
                            normalize=False)
                    vutils.save_image((rec_x+1.0)/2.0,
                            '{}/{:03d}-rec.png'.format(opt.outdir, epoch),
                            normalize=False)
                    fake = gen(fixed_z).data
                    vutils.save_image((fake+1.0)/2.0,
                            '{}/{:03d}-gen.png'.format(opt.outdir, epoch),
                            normalize=True)

            # validation dump
            metrics = None
            for valid_data in valid_dataloader:
                (valid_metrics, valid_rec) = do_batch(0, valid_data, train=False)
                batchlen = valid_data[0].size(0)
                if metrics is None:
                    metrics = [ m*batchlen for m in valid_metrics ]
                else:
                    metrics = [ oldm + m*batchlen for (oldm, m) in zip(metrics, valid_metrics) ]
            metrics = [ float(epoch) ] + [ m/len(valid_dataset) for m in metrics ]
            print(",".join("%.6f" % v for v in metrics), file=valid_log_file, flush=True)
            vutils.save_image(valid_data[0],
                        '{}/{:03d}-valid-real.png'.format(opt.outdir, epoch),
                        normalize=False)
            vutils.save_image((valid_rec+1.0)/2.0,
                        '{}/{:03d}-valid-rec.png'.format(opt.outdir, epoch),
                        normalize=False)

            # dump ?
            if opt.modeldump is not None and epoch % opt.modeldump == 0:
                torch.save(gen.state_dict(), "{}/gen-{}.npz".format(opt.outdir, epoch))
                torch.save(enc.state_dict(), "{}/enc-{}.npz".format(opt.outdir, epoch))
                torch.save(disc.state_dict(), "{}/disc-{}.npz".format(opt.outdir, epoch))
