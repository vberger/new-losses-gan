## New Losses for Generative Adversarial Training

This is the code used for the experiments presented in the paper
[New Losses for Generative Adversarial Training]().

All setups can be run out of the box by providing appropriate arguments to `main.py`.

For example, a few of the possible setups:

- `main.py --use_vae` will train a regular VAE with a gaussian observation model
- `main.py --use_gan` will train a regular GAN
- `main.py --use_vae --disc_adv_depth 3` will train a VAE using the perceptual
  distance defined by the 3rd convolutional layer of the discriminator
- `main.py --use_vae --use_gan --train_disc_on_rec --use_gan_on_rec --disc_adv_depth 3` will train
  a [VAEGAN](https://arxiv.org/abs/1512.09300).
- `main.py --use_vae --use_gan --use_noise_on_gen --disc_adv_depth 2` will train the SDKL model introduced in the paper.


Complete program usage:

```
usage: main.py [-h] --dataroot DATAROOT --outdir OUTDIR
               [--batchsize BATCHSIZE] [--epochs EPOCHS] [--alpha ALPHA]
               [--disc_alpha DISC_ALPHA] [--beta1 BETA1] [--beta2 BETA2]
               [--reload RELOAD] [--imgdump IMGDUMP] [--modeldump MODELDUMP]
               [--cuda] [--hescale HESCALE] [--disc_adv_depth DISC_ADV_DEPTH]
               [--disc_adv_lambdas DISC_ADV_LAMBDAS [DISC_ADV_LAMBDAS ...]]
               [--disc_ortho DISC_ORTHO] [--use_vae] [--use_gan]
               [--train_disc_on_rec] [--use_gan_on_rec] [--use_noise_on_gen]
               [--n_disc_train N_DISC_TRAIN]

optional arguments:
  -h, --help            show this help message and exit
  --dataroot DATAROOT   path to dataset root
  --outdir OUTDIR       path to output directory
  --batchsize BATCHSIZE
                        input batch size
  --epochs EPOCHS       number of epochs to train for
  --alpha ALPHA         alpha for adam. default=0.00002
  --disc_alpha DISC_ALPHA
                        alpha for discriminator's adam. default=0.00002
  --beta1 BETA1         beta1 for adam. default=0.7
  --beta2 BETA2         beta2 for adam. default=0.999
  --reload RELOAD       reload from save number to continue training (starts
                        with this epoch number)
  --imgdump IMGDUMP     period of image dump (in iterations). no dump if
                        absent
  --modeldump MODELDUMP
                        period of model dump (in epoch). no dump is absent
  --cuda                enables cuda
  --hescale HESCALE     scale factor for henormal init. default=1.0
  --disc_adv_depth DISC_ADV_DEPTH
                        depth at which compute the adversarial metric
  --disc_adv_lambdas DISC_ADV_LAMBDAS [DISC_ADV_LAMBDAS ...]
                        lambda factors for combining several layers
  --disc_ortho DISC_ORTHO
                        weight of orthogonal regularisation for discriminator
  --use_vae             use the vae loss
  --use_gan             use the gan loss
  --train_disc_on_rec   show reconstructed images to the discriminator
  --use_gan_on_rec      use the gan loss on reconstructed images
  --use_noise_on_gen    apply the VAE noise model on GAN generation
  --n_disc_train N_DISC_TRAIN
                        number of trainig of the discriminator compared to
                        generator
```


